# WatersMV API

An API layer for the PostgreSQL database. The application is based
on [Django REST framework][django-rest-framework].


## Setup

The following steps have to be made to setup the application.

### Dependencies

The following dependencies must be installed before the application can run.

* postgresql
* postgresql-contrib
* postgresql-server-dev-9.4
* python-dev
* python-psycopg2
* python-virtualenv


### Virtual environment

Create a virtual environment using [virtualenv][virtualenv] for the
application to encapsulate Python packages. The command automatically creates a new folder named `watersmvapi`.

```bash
$ virtualenv watersmvapi
$ cd watersmvapi
```

Make sure the virtual environment is activated. This can be done via:

```bash
$ source bin/activate
(watersmvapi)$
```


### Clone the repository

Clone the repository into the root folder of the virtual environment.
The folder structure should look like the following afterwards.

```
.
├── bin
│   └── activate
├── include
├── lib
├── local
└── watersmvapi
    ├── example.env
    ├── gaugereports
    ├── manage.py
    ├── README.md
    ├── requirements.txt
    └── watersmvapi
```


### Install packages

Install all required Python packages.

```bash
$ cd watersmvapi
(watersmvapi)$ pip install -r requirements.txt --ignore-installed
```

### Database

In PostgreSQL create a database user and a new database owned by this user.


### Configuration

Before the application can run a few settings have to be made. Therefore
create a copy of `example.env` and rename the new file to `.env`. Then, in
`.env` add values for `ENVIRONMENT`, `DJANGO_SECRET_KEY` and the database
connection. These settings are automatically read from the Django application.

```
.
├── bin
│   └── activate
└── watersmvapi
    ├── .env
    ├── example.env
    ├── gaugereports
    ├── manage.py
    ├── README.md
    ├── requirements.txt
    └── watersmvapi
```


### Database migration

Initialize the database.

```bash
(watersmvapi)$ python manage.py syncdb
```

To run pending migrations execute the following command.

```bash
(watersmvapi)$ python manage.py migrate
```


## Authors

* Tobias Preuss


[virtualenv]: https://virtualenv.pypa.io/en/latest/
[django-rest-framework]: http://www.django-rest-framework.org
