# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    replaces = [(b'gaugereports', '0001_initial'), (b'gaugereports', '0002_auto_20150115_1451')]

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GaugeReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('water_level', models.IntegerField(max_length=5)),
                ('user_name', models.CharField(max_length=256)),
                ('email_address', models.CharField(max_length=256, blank=True)),
                ('recorded_at', models.DateTimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='gaugereport',
            options={'ordering': ('modified_at',)},
        ),
    ]
