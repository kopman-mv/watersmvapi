# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gaugereports', '0001_squashed_0002_auto_20150115_1451'),
    ]

    operations = [
        migrations.AddField(
            model_name='gaugereport',
            name='feature_id',
            field=models.IntegerField(default=None, max_length=10),
            preserve_default=True,
        ),
    ]
