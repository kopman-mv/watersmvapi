# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gaugereports', '0002_gaugereport_feature_id'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='gaugereport',
            options={'ordering': ('-recorded_at',)},
        ),
    ]
