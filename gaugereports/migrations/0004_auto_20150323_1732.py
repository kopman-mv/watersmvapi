# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gaugereports', '0003_auto_20150211_1142'),
    ]

    operations = [
        migrations.CreateModel(
            name='Report',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('feature_id', models.IntegerField(default=None, max_length=10)),
                ('water_level', models.IntegerField(max_length=5)),
                ('user_name', models.CharField(max_length=256)),
                ('email_address', models.CharField(max_length=256, blank=True)),
                ('recorded_at', models.DateTimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('modified_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ('-recorded_at',),
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='GaugeReport',
        ),
    ]
