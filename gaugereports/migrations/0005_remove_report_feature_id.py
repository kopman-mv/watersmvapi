# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gaugereports', '0004_auto_20150323_1732'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='report',
            name='feature_id',
        ),
    ]
