# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gaugereports', '0007_auto_20150323_1812'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='gauge_photo',
            field=models.ImageField(upload_to=b'', blank=True),
            preserve_default=True,
        ),
    ]
