#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

class Gauge(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now_add=True)


class Report(models.Model):
	water_level = models.IntegerField(max_length=5, blank=False)
	user_name = models.CharField(max_length=256, blank=False)
	email_address = models.CharField(max_length=256, blank=True)
	recorded_at = models.DateTimeField(blank=False)
	gauge_photo = models.ImageField(blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now_add=True)
	gauge = models.ForeignKey('Gauge', default=None)

	class Meta:
		ordering = ('-recorded_at',)


class State(models.Model):
	name = models.CharField(max_length=256, blank=False)
	code = models.IntegerField(max_length=5, blank=False)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now_add=True)


class Review(models.Model):
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now_add=True)
	report = models.ForeignKey('Report', default=None)
	state = models.ForeignKey('State', default=None)

	class Meta:
		ordering = ('-created_at',)


class Property(models.Model):
	database_column_name = models.CharField(max_length=256, blank=False)
	translation_de_de = models.CharField(max_length=256, blank=False)
	translation_en_us = models.CharField(max_length=256, blank=False)
	created_at = models.DateTimeField(auto_now_add=True)
	modified_at = models.DateTimeField(auto_now_add=True)
