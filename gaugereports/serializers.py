#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from models import Gauge, Report, State, Review, Property


class GaugeSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Gauge


class ReportSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Report


class StateSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = State


class ReviewSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Review


class PropertySerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Property
        fields = ('database_column_name', 'translation_de_de', 'translation_en_us')
