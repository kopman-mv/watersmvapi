#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render
from rest_framework import viewsets, mixins
from models import Gauge, Report, State, Review, Property
from serializers import GaugeSerializer, ReportSerializer, StateSerializer, ReviewSerializer, PropertySerializer
from rest_framework.decorators import detail_route
from rest_framework.response import Response


class GaugeViewSet(mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    """
    API endpoint that allows gauge to be created or viewed.
    """
    queryset = Gauge.objects.all()
    serializer_class = GaugeSerializer

    @detail_route(methods=['get'])
    def reports(self, request, pk=None):
        gauge = self.get_object()
        reports = gauge.report_set.all()
        serializer = ReportSerializer(reports, context={'request': request}, many=True)
        return Response(serializer.data)


class ReportViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    """
    API endpoint that allows gauge reports to be created or viewed.
    """
    queryset = Report.objects.all()
    serializer_class = ReportSerializer

    @detail_route(methods=['get'])
    def reviews(self, request, pk=None):
        report = self.get_object()
        reviews = report.review_set.all()
        serializer = ReviewSerializer(reviews, context={'request': request}, many=True)
        return Response(serializer.data)


class StateViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   viewsets.GenericViewSet):
    """
    API endpoint that allows review states to be created or viewed.
    """
    queryset = State.objects.all()
    serializer_class = StateSerializer


class ReviewViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    """
    API endpoint that allows reviews to be created or viewed.
    """
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


class PropertyViewSet(mixins.RetrieveModelMixin,
                      mixins.ListModelMixin,
                      viewsets.GenericViewSet):
    """
    API endpoint that allows feature properties to be viewed.
    """
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
