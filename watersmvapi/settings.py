"""
Django settings for watersmvapi project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os

from configurations import Configuration, values


class Common(Configuration):


    BASE_DIR = os.path.dirname(os.path.dirname(__file__))


    # Quick-start development settings - unsuitable for production
    # See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = values.SecretValue()

    # SECURITY WARNING: don't run with debug turned on in production!
    DEBUG = values.BooleanValue(False)

    TEMPLATE_DEBUG = values.BooleanValue(DEBUG)

    ALLOWED_HOSTS = []


    # Application definition

    INSTALLED_APPS = (
        'django.contrib.admin',
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        'corsheaders',
        'rest_framework',
        'gaugereports'
    )

    MIDDLEWARE_CLASSES = (
        'django.contrib.sessions.middleware.SessionMiddleware',
        'corsheaders.middleware.CorsMiddleware',
        'django.middleware.common.CommonMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
        'django.middleware.clickjacking.XFrameOptionsMiddleware',
    )

    ROOT_URLCONF = 'watersmvapi.urls'

    WSGI_APPLICATION = 'watersmvapi.wsgi.application'


    # Database
    # https://docs.djangoproject.com/en/1.7/ref/settings/#databases

    DATABASES = {
        'default': {
            'ENGINE': os.getenv('DATABASE_ENGINE'),
            'NAME': os.getenv('DATABASE_NAME'),
            'USER': os.getenv('DATABASE_USER'),
            'PASSWORD': os.getenv('DATABASE_PASSWORD'),
            'HOST': os.getenv('DATABASE_HOST'),
            'PORT': os.getenv('DATABASE_PORT'),
        }
    }

    # Internationalization
    # https://docs.djangoproject.com/en/1.7/topics/i18n/

    LANGUAGE_CODE = 'en-us'

    TIME_ZONE = 'Europe/Berlin'

    USE_I18N = True

    USE_L10N = True

    USE_TZ = True


    # Static files (CSS, JavaScript, Images)
    # https://docs.djangoproject.com/en/1.7/howto/static-files/
    """
    Must match with site configuration in Nginx
    """
    STATIC_URL = '/watersmvapi/static/'

    """
    Enable CORS headers for /watersmvapi/
    """
    CORS_URLS_REGEX = r'^/watersmvapi/.*$'


class Development(Common):
    """
    The development settings and the default configuration.
    """
    DEBUG = True

    TEMPLATE_DEBUG = True

    ENV_PATH = os.path.abspath(os.path.dirname(__file__))

    """
    Absolute filesystem path to the directory that will hold user-uploaded files.
    """
    MEDIA_ROOT = os.path.join(ENV_PATH, 'media/')

    """
    Relative browser URL you should access your media files from
    """
    MEDIA_URL = '/media/'


class Production(Common):
    """
    The production settings and the default configuration.
    """
    ALLOWED_HOSTS = [
        '178.63.73.240',
        '178.63.73.240.'
    ]

    """
    Move application to subpath
    """
    FORCE_SCRIPT_NAME = '/watersmvapi'

    """
    Must match with site configuration in Nginx
    """
    STATIC_ROOT = '/webapps/watersmvapi/static/'

    """
    Absolute filesystem path to the directory that will hold user-uploaded files.
    """
    MEDIA_ROOT = '/webapps/watersmvapi/media/'

    """
    Relative browser URL you should access your media files from
    Path must be served from Nginx configuration
    """
    MEDIA_URL = '/watersmvapi/media/'
