from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from gaugereports.views import GaugeViewSet, ReportViewSet, StateViewSet, ReviewViewSet, PropertyViewSet
import settings
from django.conf.urls.static import static


router = DefaultRouter()
router.register(r'gauges', GaugeViewSet)
router.register(r'reports', ReportViewSet)
router.register(r'states', StateViewSet)
router.register(r'reviews', ReviewViewSet)
router.register(r'properties', PropertyViewSet)


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'watersmvapi.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
